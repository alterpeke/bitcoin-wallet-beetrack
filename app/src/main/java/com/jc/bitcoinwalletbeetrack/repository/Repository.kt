package com.jc.bitcoinwalletbeetrack.repository

import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.model.response.Transactions
import com.jc.bitcoinwalletbeetrack.model.service.IApiSource

class Repository(private val apiSource: IApiSource) : IRepository {
    override suspend fun getAddress(): Address = apiSource.postAddress()
    override suspend fun getBalance(publicAddress: String): Balance = apiSource.getBalance(publicAddress)
    override suspend fun getTransactions(publicAddress: String): Transactions = apiSource.getTransactions(publicAddress)
}