package com.jc.bitcoinwalletbeetrack.repository

import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.model.response.Transactions

interface IRepository {
    suspend fun getAddress(): Address
    suspend fun getBalance(publicAddress: String): Balance
    suspend fun getTransactions(publicAddress: String): Transactions
}