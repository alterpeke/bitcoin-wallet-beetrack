package com.jc.bitcoinwalletbeetrack

import android.app.Application
import com.jc.bitcoinwalletbeetrack.di.Modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // use the Android context given there
            androidContext(this@MyApplication)
            // module list
            modules(Modules.all)
        }
    }
}