package com.jc.bitcoinwalletbeetrack.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jc.bitcoinwalletbeetrack.model.api.ApiEndPoints.BASE_URL
import com.jc.bitcoinwalletbeetrack.model.api.IApiService
import com.jc.bitcoinwalletbeetrack.model.service.ApiSource
import com.jc.bitcoinwalletbeetrack.repository.Repository
import com.jc.bitcoinwalletbeetrack.ui.address.AddressMakerViewModel
import com.jc.bitcoinwalletbeetrack.ui.history.TransactionHistoryViewModel
import com.jc.bitcoinwalletbeetrack.ui.wallet.WalletStateViewModel
import com.jc.bitcoinwalletbeetrack.use_case.address.AddressMakerUseCase
import com.jc.bitcoinwalletbeetrack.use_case.transaction.TransactionHistoryUseCase
import com.jc.bitcoinwalletbeetrack.use_case.wallet.WalletStateUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Modules {

    private val apiModule = module {
        single {
            val retrofit: Retrofit = get()
            retrofit.create(IApiService::class.java)
        }

        single {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }
    }

    private val repositoryModule = module {
        single { Repository(ApiSource(get())) }
    }

    private val useCaseModule = module {
        single { AddressMakerUseCase(repository = get()) }
        single { WalletStateUseCase(repository = get()) }
        single { TransactionHistoryUseCase(repository = get()) }
    }

    private val viewModelModule = module {
        viewModel { AddressMakerViewModel(addressMakerUseCase = get()) }
        viewModel { WalletStateViewModel(walletStateUseCase = get()) }
        viewModel { TransactionHistoryViewModel(transactionHistoryUseCase = get()) }
    }

    val all: List<Module> = listOf(
        apiModule,
        repositoryModule,
        useCaseModule,
        viewModelModule
    )
}