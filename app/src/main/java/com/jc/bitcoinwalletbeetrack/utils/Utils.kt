package com.jc.bitcoinwalletbeetrack.utils

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import com.jc.bitcoinwalletbeetrack.ui.address.AddressMakerFragment
import com.journeyapps.barcodescanner.BarcodeEncoder

object Utils {
    fun drawQrCode(stringToDraw: String): Bitmap {
        val barcodeEncoder = BarcodeEncoder()
        val bitmap =
            barcodeEncoder.encodeBitmap(
                stringToDraw, BarcodeFormat.QR_CODE,
                300,
                300
            )

        return bitmap
    }
}