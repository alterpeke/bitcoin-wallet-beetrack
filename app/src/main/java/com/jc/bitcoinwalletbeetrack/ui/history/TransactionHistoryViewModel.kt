package com.jc.bitcoinwalletbeetrack.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jc.bitcoinwalletbeetrack.model.response.Transactions
import com.jc.bitcoinwalletbeetrack.use_case.transaction.TransactionHistoryUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class TransactionHistoryViewModel(private val transactionHistoryUseCase: TransactionHistoryUseCase) :
    ViewModel() {

    private val transactionsLiveData = MutableLiveData<TransactionHistoryState>()
    private val coroutineExceptionHandler = CoroutineExceptionHandler{ _, t ->
        run {
            t.printStackTrace()
        }
    }

    fun init(publicString: String) {
        fetchTransactions(publicString)
    }

    private fun fetchTransactions(publicString: String) {
        transactionsLiveData.value = TransactionHistoryState.Loading
        viewModelScope.launch(coroutineExceptionHandler) {
            try {
                val transactions = async {
                    transactionHistoryUseCase.getTransactions(publicString)
                }.await()
                transactionsLiveData.value = TransactionHistoryState.Success(transactions)
            } catch (exception: Exception) {
                transactionsLiveData.value =
                    TransactionHistoryState.Error("Error Retrieving Transactions")
            }
        }
    }

    fun getTransactions(): LiveData<TransactionHistoryState> = transactionsLiveData
}

sealed class TransactionHistoryState {
    object Loading : TransactionHistoryState()
    data class Success(val transactions: Transactions) : TransactionHistoryState()
    data class Error(val message: String) : TransactionHistoryState()
}