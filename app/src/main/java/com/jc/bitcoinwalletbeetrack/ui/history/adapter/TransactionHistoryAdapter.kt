package com.jc.bitcoinwalletbeetrack.ui.history.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jc.bitcoinwalletbeetrack.R
import com.jc.bitcoinwalletbeetrack.model.response.Transaction
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


class TransactionHistoryAdapter(
    private val transactions: ArrayList<Transaction>
) : RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder>() {

    private val numberFormat: NumberFormat = NumberFormat.getNumberInstance()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDate: TextView = itemView.findViewById(R.id.tv_transaction_date)
        val tvAmount: TextView = itemView.findViewById(R.id.tv_transaction_amount)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        numberFormat.maximumFractionDigits = 0
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_transaction_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvDate.text = getDate(transactions[position].received) //TODO format date
        holder.tvAmount.text = numberFormat.format(transactions[position].total)
    }

    private fun getDate(dateStr: String): String {
        return try {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
            //TODO warning
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            val mDate = formatter.format(parser.parse(dateStr))
            mDate.toString()
        } catch (e: Exception) {
            "2022-01-01" //TODO make a proper treatment of the exception
        }
    }

    override fun getItemCount(): Int {
        return transactions.size
    }
}