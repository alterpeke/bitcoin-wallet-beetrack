package com.jc.bitcoinwalletbeetrack.ui.address

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.use_case.address.AddressMakerUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class AddressMakerViewModel(private val addressMakerUseCase: AddressMakerUseCase) : ViewModel() {

    private val addressLiveData = MutableLiveData<AddressState>()
    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, t ->
        run {
            t.printStackTrace()
        }
    }

    fun generateAddress() {
        fetchAddress()
    }

    private fun fetchAddress() {
        addressLiveData.value = AddressState.Loading
        viewModelScope.launch(coroutineExceptionHandler) {
            try {
                val address = async {
                    addressMakerUseCase.getAddress()
                }.await()
                addressLiveData.value = AddressState.Success(address)
            } catch (exception: Exception) {
                addressLiveData.value = AddressState.Error("Error retrieving address")
            }
        }
    }

    fun getAddress(): LiveData<AddressState> {
        return addressLiveData
    }
}

sealed class AddressState {
    object Loading : AddressState()
    data class Success(val address: Address) : AddressState()
    data class Error(val message: String) : AddressState()
}
