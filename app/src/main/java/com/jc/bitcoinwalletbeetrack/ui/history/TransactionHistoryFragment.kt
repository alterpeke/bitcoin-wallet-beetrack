package com.jc.bitcoinwalletbeetrack.ui.history

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jc.bitcoinwalletbeetrack.R
import com.jc.bitcoinwalletbeetrack.databinding.FragmentTransactionHistoryBinding
import com.jc.bitcoinwalletbeetrack.model.response.Transaction
import com.jc.bitcoinwalletbeetrack.ui.history.adapter.TransactionHistoryAdapter
import org.koin.android.ext.android.inject

class TransactionHistoryFragment : Fragment() {

    private var _binding: FragmentTransactionHistoryBinding? = null
    private val viewModel: TransactionHistoryViewModel by inject()
    private lateinit var adapter: TransactionHistoryAdapter
    private var address: String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        address = sharedPref?.getString(
            getString(R.string.shared_preference_key_address),
            getString(R.string.default_address)
        )
        address?.let { viewModel.init(it) }

        _binding = FragmentTransactionHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getTransactions().observe(viewLifecycleOwner) {
            onStateChanged(it)
        }

        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_TransactionHistoryFragment_to_WalletStateFragment)
        }
    }

    private fun onStateChanged(transactionHistoryState: TransactionHistoryState) =
        when (transactionHistoryState) {
            is TransactionHistoryState.Error -> showError()
            TransactionHistoryState.Loading -> binding.progressBar.isVisible = true
            is TransactionHistoryState.Success -> showTransactions(transactionHistoryState.transactions.txs)
        }

    private fun showError() {
        binding.progressBar.isVisible = false
        binding.rvList.isVisible = false
        Toast.makeText(activity, getString(R.string.error), Toast.LENGTH_LONG).show()
    }

    private fun showTransactions(transactions: List<Transaction>) {
        binding.progressBar.isVisible = false
        binding.rvList.isVisible = true
        if (!transactions.isNullOrEmpty()) {
            binding.rvList.setHasFixedSize(true)
            binding.rvList.layoutManager = LinearLayoutManager(activity)
            adapter = TransactionHistoryAdapter(ArrayList(transactions))
            binding.rvList.adapter = adapter
        } else {
            Toast.makeText(activity, getString(R.string.no_data), Toast.LENGTH_LONG).show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}