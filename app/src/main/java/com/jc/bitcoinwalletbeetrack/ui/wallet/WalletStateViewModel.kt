package com.jc.bitcoinwalletbeetrack.ui.wallet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.use_case.wallet.WalletStateUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class WalletStateViewModel(private val walletStateUseCase: WalletStateUseCase) : ViewModel() {

    private val balanceLiveData = MutableLiveData<BalanceState>()
    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, t ->
        run {
            t.printStackTrace()
        }
    }

    fun init(publicAddress: String) {
        fetchBalance(publicAddress)
    }

    private fun fetchBalance(publicAddress: String) {
        balanceLiveData.value = BalanceState.Loading
        viewModelScope.launch(coroutineExceptionHandler) {
            try {
                val balance = async {
                    walletStateUseCase.getBalance(publicAddress)
                }.await()
                balanceLiveData.value = BalanceState.Success(balance)
            } catch (exception: Exception) {
                balanceLiveData.value = BalanceState.Error("Error retrieving Balance")
            }
        }
    }

    fun getBalance(): LiveData<BalanceState> = balanceLiveData

}

sealed class BalanceState {
    object Loading : BalanceState()
    data class Success(val balance: Balance) : BalanceState()
    data class Error(val message: String) : BalanceState()
}