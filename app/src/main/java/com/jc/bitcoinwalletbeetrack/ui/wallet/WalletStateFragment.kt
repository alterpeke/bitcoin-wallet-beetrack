package com.jc.bitcoinwalletbeetrack.ui.wallet

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jc.bitcoinwalletbeetrack.R
import com.jc.bitcoinwalletbeetrack.databinding.FragmentWalletStateBinding
import com.jc.bitcoinwalletbeetrack.utils.Utils
import org.koin.android.ext.android.inject


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class WalletStateFragment : Fragment() {

    private var _binding: FragmentWalletStateBinding? = null
    private val viewModel: WalletStateViewModel by inject()
    private var _address: String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        _address = sharedPref?.getString(
            getString(R.string.shared_preference_key_address),
            getString(R.string.default_address)
        )

        _address?.let { viewModel.init(it) }

        _binding = FragmentWalletStateBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getBalance().observe(viewLifecycleOwner) {
            onStateChanged(it)
        }

        binding.buttonTransaction.setOnClickListener {
            findNavController().navigate(R.id.action_WalletStateFragment_to_TransactionHistoryFragment)
        }

        binding.buttonDelete.setOnClickListener {
            deleteSharedPreferenceEntry()
            findNavController().navigate(R.id.action_WalletStateFragment_to_AddressMakerFragment)
        }
    }

    private fun onStateChanged(balanceState: BalanceState) = when (balanceState) {
        is BalanceState.Error -> showError()
        BalanceState.Loading -> showLoading()
        is BalanceState.Success -> showBalance(
            balanceState.balance.balance,
            balanceState.balance.final_balance,
            balanceState.balance.unconfirmed_balance
        )
    }

    private fun showError() {
        binding.tvBalance.text = getString(R.string.address_error_message)
    }

    private fun showLoading() {
        with(binding) {
            this.viewQrCode.isVisible = true
            this.viewQrCode.isVisible = false
            this.tvAddress.isVisible = false
        }
    }

    private fun showBalance(balance: Int, finalBalance: Int, unconfirmedBalance: Int) {
        with(binding) {
            this.viewQrCode.isVisible = false
            this.viewQrCode.isVisible = true
            this.tvAddress.isVisible = true
            _address?.let {
                this.viewQrCode.setImageBitmap(Utils.drawQrCode(it))
                this.tvAddress.text = it
            }
            this.tvBalance.text = getString(R.string.balance_value, balance)
            this.tvUnconfirmedBalance.text =
                getString(R.string.unconfirmed_balance_value, unconfirmedBalance)
            this.tvFinalBalance.text = getString(R.string.final_balance_value, finalBalance)
        }
    }

    private fun deleteSharedPreferenceEntry() {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        with(sharedPref?.edit()) {
            this?.putString(
                getString(R.string.shared_preference_key_address),
                null
            )
            this?.apply()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}