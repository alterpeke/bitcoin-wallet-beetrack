package com.jc.bitcoinwalletbeetrack.ui.address

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jc.bitcoinwalletbeetrack.R
import com.jc.bitcoinwalletbeetrack.databinding.FragmentAddressMakerBinding
import com.jc.bitcoinwalletbeetrack.utils.Utils
import org.koin.android.ext.android.inject

class AddressMakerFragment : Fragment() {

    private var _binding: FragmentAddressMakerBinding? = null
    private val viewModel: AddressMakerViewModel by inject()
    private var _address: String? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (!getSavedAddressSharedPreferences().isNullOrBlank()) {
            findNavController().navigate(R.id.action_AddressMakerFragment_to_WalletStateFragment)
        } else {
            viewModel.getAddress().observe(viewLifecycleOwner) {
                onStateChanged(it)
            }
        }

        _binding = FragmentAddressMakerBinding.inflate(inflater, container, false)
        return binding.root

    }

    private fun onStateChanged(state: AddressState) = when (state) {
        is AddressState.Error -> showError()
        AddressState.Loading -> showLoading()
        is AddressState.Success -> showAddress(state.address.address)
    }

    private fun showLoading() {
        binding.progressBar.isVisible = true
        binding.viewQrCode.isVisible = false
        binding.tvAddress.isVisible = false
    }

    private fun showError() {
        binding.tvAddress.text = getString(R.string.address_error_message)
    }

    private fun showAddress(address: String) {
        binding.progressBar.isVisible = false
        binding.tvAddress.isVisible = true
        binding.viewQrCode.isVisible = true
        binding.viewQrCode.setImageBitmap(Utils.drawQrCode(address))
        binding.tvAddress.text = address
        _address = address
    }

    private fun saveAddressIntoSharedPreferences(address: String) {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(
                getString(R.string.shared_preference_key_address),
                address
            )
            apply()
        }
    }

    private fun getSavedAddressSharedPreferences(): String? {
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        return sharedPref?.getString(
            getString(R.string.shared_preference_key_address),
            null
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonGenerateAddress.setOnClickListener {
            viewModel.generateAddress()
        }

        binding.buttonSaveAddress.setOnClickListener {
            val builder = AlertDialog.Builder(activity)
            builder.setMessage(getString(R.string.dialog_confirm_question))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.dialog_confirm_yes)) { _, _ ->
                    _address?.let { saveAddressIntoSharedPreferences(it) }
                    findNavController().navigate(R.id.action_AddressMakerFragment_to_WalletStateFragment)
                }
                .setNegativeButton(getString(R.string.dialog_confirm_no)) { dialog, id ->
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}