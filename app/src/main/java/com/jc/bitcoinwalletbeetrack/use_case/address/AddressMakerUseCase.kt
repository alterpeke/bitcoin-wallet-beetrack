package com.jc.bitcoinwalletbeetrack.use_case.address

import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AddressMakerUseCase(private val repository: Repository) : IAddressMakerUseCase {
    override suspend fun getAddress(): Address =
        withContext(Dispatchers.IO) {
            repository.getAddress()
        }
}