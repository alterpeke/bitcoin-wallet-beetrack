package com.jc.bitcoinwalletbeetrack.use_case.wallet

import com.jc.bitcoinwalletbeetrack.model.response.Balance

interface IWalletStateUseCase {
    suspend fun getBalance(publicAddress: String): Balance
}