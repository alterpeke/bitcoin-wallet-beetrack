package com.jc.bitcoinwalletbeetrack.use_case.transaction

import com.jc.bitcoinwalletbeetrack.model.response.Transactions

interface ITransactionHistoryUseCase {
    suspend fun getTransactions(publicAddress: String): Transactions
}