package com.jc.bitcoinwalletbeetrack.use_case.transaction

import com.jc.bitcoinwalletbeetrack.model.response.Transactions
import com.jc.bitcoinwalletbeetrack.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TransactionHistoryUseCase(private val repository: Repository) : ITransactionHistoryUseCase {
    override suspend fun getTransactions(publicAddress: String): Transactions =
        withContext(Dispatchers.IO) {
            repository.getTransactions(publicAddress)
        }
}