package com.jc.bitcoinwalletbeetrack.use_case.wallet

import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WalletStateUseCase(private val repository: Repository) : IWalletStateUseCase {
    override suspend fun getBalance(publicAddress: String): Balance =
        withContext(Dispatchers.IO) {
            repository.getBalance(publicAddress)
        }
}