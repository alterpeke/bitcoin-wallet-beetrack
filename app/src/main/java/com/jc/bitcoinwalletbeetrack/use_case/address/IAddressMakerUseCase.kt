package com.jc.bitcoinwalletbeetrack.use_case.address

import com.jc.bitcoinwalletbeetrack.model.response.Address

interface IAddressMakerUseCase {
    suspend fun getAddress(): Address
}