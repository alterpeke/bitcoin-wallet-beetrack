package com.jc.bitcoinwalletbeetrack.model.service

import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.model.response.Transactions

interface IApiSource {
    suspend fun postAddress(): Address
    suspend fun getBalance(publicAddress: String): Balance
    suspend fun getTransactions(publicAddress: String): Transactions
}