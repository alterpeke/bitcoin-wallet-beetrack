package com.jc.bitcoinwalletbeetrack.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address (
    val private : String,
    val public : String,
    val address : String,
    val wif : String
    ): Parcelable
