package com.jc.bitcoinwalletbeetrack.model.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Outputs(
    val value: Double,
    val script: String,
    val addresses: List<String>,
    val script_type: String
) : Parcelable
