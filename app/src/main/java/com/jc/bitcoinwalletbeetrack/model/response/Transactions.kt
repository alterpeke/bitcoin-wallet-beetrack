package com.jc.bitcoinwalletbeetrack.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Transactions(
    val address: String,
    val total_received: Double,
    val total_sent: Double,
    val balance: Double,
    val unconfirmed_balance: Double,
    val final_balance: Double,
    val n_tx: Double,
    val unconfirmed_n_tx: Double,
    val final_n_tx: Double,
    val hasMore: Boolean,
    val txs: List<Transaction>
) : Parcelable


