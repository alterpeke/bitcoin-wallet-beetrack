package com.jc.bitcoinwalletbeetrack.model.api

import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.model.response.Transactions
import retrofit2.http.*

interface IApiService {
    @POST("addrs")
    suspend fun postAddress(): Address

    @GET("addrs/{address}/balance")
    suspend fun getBalance(@Path("address") address: String): Balance

    @GET("addrs/{address}/full")
    suspend fun getTransactions(@Path("address") address: String): Transactions
}