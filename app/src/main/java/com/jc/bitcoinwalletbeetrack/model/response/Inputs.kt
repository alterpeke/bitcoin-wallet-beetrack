package com.jc.bitcoinwalletbeetrack.model.response

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Inputs(
    val prev_hash: String,
    val output_index: Double,
    val output_value: Double,
    val sequence: Double,
    val addresses: List<String>,
    val script_type: String,
    val age: Double,
    val witness: List<String>
) : Parcelable
