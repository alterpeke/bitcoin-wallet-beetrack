package com.jc.bitcoinwalletbeetrack.model.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Transaction(
    val block_hash: String,
    val block_height: Double,
    val block_index: Double,
    val hash: String,
    val addresses: List<String>,
    val total: Double,
    val fees: Double,
    val size: Double,
    val vsize: Double,
    val preference: String,
    val confirmed: String,
    val received: String,
    val ver: Double,
    val double_spend: Boolean,
    val vin_sz: Double,
    val vout_sz: Double,
    val confirmations: Double,
    val confidence: Double,
    val inputs: List<Inputs>,
    val outputs: List<Outputs>
) : Parcelable

