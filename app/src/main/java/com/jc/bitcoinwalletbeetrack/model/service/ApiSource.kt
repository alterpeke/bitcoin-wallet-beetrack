package com.jc.bitcoinwalletbeetrack.model.service

import com.jc.bitcoinwalletbeetrack.model.response.Address
import com.jc.bitcoinwalletbeetrack.model.response.Balance
import com.jc.bitcoinwalletbeetrack.model.response.Transactions
import com.jc.bitcoinwalletbeetrack.model.api.IApiService


class ApiSource(private val apiService: IApiService) : IApiSource {
    override suspend fun postAddress(): Address = apiService.postAddress()
    override suspend fun getBalance(publicAddress: String): Balance = apiService.getBalance(publicAddress)
    override suspend fun getTransactions(publicAddress: String): Transactions = apiService.getTransactions(publicAddress)
}